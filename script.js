/*
1. Які існують типи даних у Javascript?
    У JS існує 8 типів даних. Це примітивні: number (число), bigint (велике число), рядок (string), булеве значення (boolean), 
    null, undefined, а також об'єкт (object) та symbol.

2. У чому різниця між == і ===?
    Під час використання оператора нестрогої рівності == здійснюється неявне перетворення типу операндів на числа та їх порівняння.
    З використанням оператора строгої рівності === неявного перетворення типу не відбувається. 

3. Що таке оператор?
    Оператор застосовується до операндів. Залежно від кількості операндів, буває унарний (якщо операнд один, наприклад, -4), 
    бінарний (якщо застосовується до двох операндів, наприклад, 9 - 4) та тернарний (умовний оператор).
    Оператори бувають математичні (+, -, /, *, %, **), порівняння (>, <, ==, !=, >=, <=), логічні (|| або, && і, ! не) та ін. */


   "use strict";

//    оголошення, отримання і перевірка введення імені користувача
   let name = prompt("What is your name?");
   while (!name) {
   alert('Please, enter your name');
   name = prompt("What is your name?", name);
   }

//    оголошення, отримання і перевірка правильності введення віку користувача
   let age = +prompt("How old are you?");
   while (Number.isInteger(age) == false) {
        alert('Please, enter your age');
        age = +prompt("How old are you?", age);
   }    
   
// Виконання умов залежно від введеного віку користувача
   if (age<18) {
       alert("You are not allowed to visit this website");
   } else if (age>=18 && age<=22) {
     let userAnswer = confirm("Are you sure you want to continue?");
        if (userAnswer == true) {
            alert(`Welcome, ${name}`);
        } else {
                alert("You are not allowed to visit this website");
        }
   } else {
       alert(`Welcome, ${name}`);
   }

   
  
